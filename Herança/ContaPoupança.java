public class ContaPoupança extends Conta{
    private int diaAniversario;
    private double taxaDeJuros;

    public ContaPoupança(int numero, int agencia, String banco, double saldo, int diaAniversario, double taxaDeJuros) {
        super(numero, agencia, banco, saldo);
        this.diaAniversario = diaAniversario;
        this.taxaDeJuros = taxaDeJuros;
    }

    public double getSaldo() {

        return this.saldo = this.saldo + this.saldo *this.taxaDeJuros*this.saldo;
    }

    public double sacar(double valor) {
        setSaldo(getSaldo() - valor);
        return valor;
    }

    @Override
    public double depositar(double valor) {
        setSaldo(getSaldo() + valor);
        return valor;
    }

    @Override
    public String toString() {
        return  super.toString() + "ContaPoupança [diaAniversario=" + diaAniversario + ", taxaDeJuros=" + taxaDeJuros + "]";
    }
}
