public class TestaConta {
    public static void main(String[] args){
        Conta contas[] = new Conta[3];

        ContaCorrente cc1 = new ContaCorrente(22, 1, "Banco AA", 1500, 700);
        ContaPoupança cp1 = new ContaPoupança(23,1, "Banco AA",500,16,0.09);
        ContaSalario cs1 = new ContaSalario(24,1,"Banco AA",1500,5);

        contas[0] = cc1;
        contas[1] = cp1;
        contas[2] = cs1;

        cs1.sacar(100);
        cs1.sacar(20);
        cs1.sacar(150);
        cs1.sacar(10);
        cs1.sacar(20);
        cs1.sacar(25);
        cs1.sacar(45);

        System.out.println("Saldo em contas: ");
        for (Conta conta:contas) {
            System.out.println(conta);
            System.out.println("Saldo atual: R$" + conta.getSaldo());
            System.out.println("-----");
        }
    }
}
