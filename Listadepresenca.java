package DB;

import java.util.ArrayList;
import java.util.HashMap;

public class Exercicio6 {
    public static void main(String[] args){
    ArrayList<String> listaNomes = new ArrayList<>();
    listaNomes.add("Emanuele");
    listaNomes.add("Ana");
    listaNomes.add("Maria");
    listaNomes.add("Alice");
    listaNomes.add("José");
    listaNomes.add("Felipe");

        HashMap<String,String> map = new HashMap<>();
        map.put("01/04/2022", "Ausente");
        map.put("02/04/2022", "Presente");
        map.put("03/04/2022", "Presente");
        map.put("04/04/2022", "Ausente");
        map.put("05/04/2022", "Presente");
        map.put("06/04/2022", "Presente");

        System.out.println(listaNomes.get(2));
        System.out.println(map.get("01/04/2022"));

    }
}
